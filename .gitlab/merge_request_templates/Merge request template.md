# Description of merge request

## Characteristics of merge request
- [ ] Bug fix
- [ ] New feature
- [ ] Breaking change
- [ ] Documentation update required

## Was it tested locally?
- [ ] Yes
- [ ] No